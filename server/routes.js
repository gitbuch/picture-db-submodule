const express = require("express"),
  debug = require("debug")("api"),
  router = express.Router(),
  uuid = require('uuid');
const uploadExif = require('./upload-exif');

router.get("/pictures", (req, res) => {
  req.app.get("db").all("SELECT * from pic", [], (err, rows) => {
    res.json({err: err, data: rows});
  });
});

router.post("/picture", uploadExif.uploadWithExifAndThumbnail('file'), (req, res) => {
  const hasExif = !! req.file.exif;
  const dateTime = req.file.exif.DateTimeOriginal || Date.now() / 1000; // exif uses seconds, not mills
  const entry = [ uuid.v1(), dateTime, Buffer.from(req.file.thumbnail, 'base64'), req.file.mimetype, req.file.size, hasExif ];
  const query = "INSERT INTO pic (id, date, thumbnail, mime, size, hasExif) VALUES (?,?,?,?,?,?)";
  req.app.get("db").run(query, entry, (err, data) => {
    res.json({ err: err, data: data});
  });
});

router.delete("/picture/:id", (req, res) => {
  debug('delete for ', req.params.id);
  req.app.get("db").run("DELETE FROM pic WHERE id = ?", [req.params.id], (err, data) => {
    res.json({err: err, data: data});
  })
});


module.exports = router;
